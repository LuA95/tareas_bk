from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('proy/', include('new_app.administracion_tareas.urls')),
]
