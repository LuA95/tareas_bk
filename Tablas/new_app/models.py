from django.db import models

# Create your models here.

class TipoTarea(models.Model):
    id_tipo_tarea = models.BigAutoField(primary_key= True, db_column= 'id_tipo_tarea', verbose_name = 'Id Tipo Tarea')
    nombre = models.CharField(max_length= 50, db_column = 'nombre', verbose_name= 'Tipo Tareas')
    
    class Meta:
        db_table = 'TipoTarea'
        verbose_name = 'Tipo Tarea'
        verbose_name_plural = 'Tipos Tarea'

class Tarea(models.Model):
    id_tarea = models.BigAutoField(primary_key = True, db_column = 'id_tarea', verbose_name = 'Id Tarea')
    title = models.CharField(max_length= 50, db_column = 'title', verbose_name = 'Title')
    fecha = models.DateField(db_column = 'fecha', verbose_name= 'fecha', null = True, blank = True)
    hora = models.TimeField(db_column = 'hora', verbose_name= 'hora' , null = True, blank = True)
    id_tipo = models.ForeignKey(TipoTarea, models.DO_NOTHING, db_column='id_tipo', verbose_name= 'Tipo Tarea')
    
    class Meta:
        db_table = 'Tarea'
        verbose_name = 'Tarea'
        verbose_name_plural = 'Tareas'