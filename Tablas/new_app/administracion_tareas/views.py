from rest_framework.generics import *
from new_app.models import *
from .serialize import *
from rest_framework.response import Response

class TipoTareaAMBC(GenericAPIView):
    serializer_class = TipoTareaSerialize    
    
    def get(self, request, *args, **kwargs):
        tipoTarea = TipoTarea.objects.all()
        
        if tipoTarea:
            serializer = TipoTareaSerialize(tipoTarea, many=True)
            return Response(serializer.data, status=200)
        
        return Response(status=204)
        
    def post(self, request, *args, **kwargs):
        
        nombre = request.data.get('nombre')
        tipoTarea = TipoTarea.objects.filter(nombre=nombre)
        
        
        if tipoTarea:
            #Si existe la tarea, borra el registro
            tipoTarea.delete()
            return Response(status= 400)
        
        #Si no existe, suma un registro
        db_register = TipoTarea(nombre = nombre)
        db_register.save()
        return Response(status= 200)
        
    def put(self, request, *args, **kwargs):
        
        id = request.data.get('id_tipo_tarea') 
        nombre = request.data.get('nombre')
        
        if id:
            
            db_register = TipoTarea(id_tipo_tarea = id, nombre = nombre)
            db_register.save()
            return Response(status = 200) 
        
        return Response(status = 404)

class TareasABMC(GenericAPIView):    
    serializer_class = TareaSerialize
   
    def get(self, request, *args, **kwargs):
        tareas = Tarea.objects.all()
        
        if tareas:
            serializer = TareaSerialize(tareas, many= True)
            return Response(serializer.data, status= 200)
        
        return Response(status=204)
    
    def post(self, request, *args, **kwargs):
        
        title = request.data.get('title')
        fecha = request.data.get('fecha')
        hora = request.data.get('hora')
        id_tipo = request.data.get('id_tipo')
        
        if id_tipo:
            
            idTipo = TipoTarea.objects.filter(id_tipo_tarea=id_tipo)[0]
        
        tarea = Tarea.objects.filter(title=title)
        
        if tarea:
            #YA existe tarea
            tarea.delete()
            return Response(status= 400)
        
        db_register = Tarea(title = title, fecha = fecha, hora = hora , id_tipo = idTipo)
        db_register.save()
        return Response(status= 200)
            
    def put(self, request, *args, **kwargs):
        
        id_tarea1 = request.data.get('id_tarea')
        title = request.data.get('title')
        fecha = request.data.get('fecha')
        hora = request.data.get('hora')
        id_tipo = request.data.get('id_tipo')
        
        tarea = Tarea.objects.filter(id_tarea = id_tarea1)
        
        if tarea:
            db_register = Tarea(id_tarea = id_tarea1, title = title, fecha = fecha, hora = hora, id_tipo = id_tipo)
            db_register.save()
            return Response(status = 200) 
    
        return Response(status = 400)
