from rest_framework import serializers
from new_app.models import *

class TipoTareaSerialize(serializers.ModelSerializer):
    class Meta:
        model = TipoTarea
        fields = '__all__'
        
class TareaSerialize(serializers.ModelSerializer):

    #Forma de relacionar las dos tablas. 
    tipo_tarea = TipoTareaSerialize(source='id_tipo', read_only=True)
    
    class Meta:
        model = Tarea
        fields = ('title','tipo_tarea','fecha', 'hora')   