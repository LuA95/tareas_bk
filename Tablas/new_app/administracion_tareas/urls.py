from django.urls import path
from .views import *

#Tipo_tareas
urlpatterns = [
   
    path('tiposTareas/', TipoTareaAMBC.as_view()),
    path('tareas/', TareasABMC.as_view()),
]